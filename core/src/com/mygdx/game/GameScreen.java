package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;

public class GameScreen extends ScreenAdapter {
	
	private World world;
	private WorldTeethRenderer worldTeethRenderer; 
    public static int toolsCheck;
    
    public GameScreen(TeethMeGame teethMeGame) {
        world = new World(teethMeGame);
        worldTeethRenderer = new WorldTeethRenderer(teethMeGame, world);
        toolsCheck = 0;
    }
    
    @Override
    public void render(float delta) {
      	Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        worldTeethRenderer.render(delta); 
        	update(delta);
    }
    
    private void update (float delta) {
    		world.update(delta);
    		updateToolsDirection();
  	}            
    
    private void updateToolsDirection() {
       	if(Gdx.input.isKeyJustPressed(Keys.SPACE)) {      
       			if(toolsCheck == 0) {
       				toolsCheck = 1;
       			}else if (toolsCheck == 1) {
       				toolsCheck = 0;
       			}
        }
    }
    
}	