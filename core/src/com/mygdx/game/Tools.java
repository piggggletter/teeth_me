package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class Tools {
	
	private Vector2 position;
	private int[] picturePosition;  
    
	public Tools(int x, int y,int xPicPos,int yPicPos) {
        position = new Vector2(x, y);
        picturePosition = new int [2];
        picturePosition[0] = xPicPos;
        picturePosition[1] = yPicPos;
   }    
 
    public Vector2 getPosition() {
        return position;    
    }
    
    public void update() {
    		setPosition(Gdx.input.getX() - picturePosition[0], Gdx.input.getY() + picturePosition[1]);
    }

	private void setPosition(int x, int y) {
		position.x = x;
		position.y = y;	
	}

}
