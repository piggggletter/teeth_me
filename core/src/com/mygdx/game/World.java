package com.mygdx.game;

import com.badlogic.gdx.Gdx;

public class World {
	
	private Tools toothBrush;
	private Tools pliers;
	private Hole hole;
	private Bacteria bacteria;
	private SoundEffect sound;
	private boolean isPlayed = false;
	private float time = 0;
	public int heartRate;

	public World(TeethMeGame teethMeGame) {
        toothBrush = new Tools(100, 100, 250, 170);
        pliers = new Tools(100, 100, 180, 200);
        bacteria = new Bacteria();
        hole = new Hole();
        sound = new SoundEffect();
    }
 
    Tools getToothBrush() {
        return toothBrush;
    }
    
    Tools getPliers() {
    		return pliers;
    }
    
    Hole getHole() {
    		return hole;
    }
    Bacteria getBacteria() {
        return bacteria;
    }
    SoundEffect getSound() {
    		return sound;
    }
    
    public void update(float delta) {
    		bacteria.goodbyeBacteria(Gdx.input.getX(), Gdx.input.getY());
    		bacteria.update(delta); 
    		hole.update(delta);
    		calHeartRate(delta);
    		hole.goodbyeHole(Gdx.input.getX(), Gdx.input.getY());
    		if(GameScreen.toolsCheck == 1) {
    			pliers.update();
    		}else {
    			toothBrush.update();
    		}
    }
    
    public void calHeartRate(float delta) {
    		int countBac = 0;
    		int countHole = 0;
    		if (time >= 1) {
    			for(int i = 0 ; i < 10 ; i++) {
    				if(bacteria.getBacPosition()[0][i] != 0) {
    					countBac++;
    				}
    			}
    			for(int i = 0 ; i < 12 ; i++) {
    				if(hole.getShowHole()[0][i] != 0) {
    					countHole++;
    				}
    			}
	    	    heartRate = (countBac * 10) + (countHole * 20);
	    	}else {
	    		time += delta;
	    	}
    }
    
    public boolean isEnd() {
    		if(heartRate >= 100) {
    			return true;
    		}
    		return false;
    }
    
    public void playGameOverSound() {
    		if(isPlayed == false) {
    			sound.playEndSound();
    			isPlayed = true;
    		}
    }

}