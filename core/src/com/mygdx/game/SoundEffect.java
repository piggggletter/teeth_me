package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class SoundEffect {
	
	private Sound bacScream;
	private Sound byeHole;
	private Music music;
	private Sound endSound;

	public SoundEffect() {
		bacScream = Gdx.audio.newSound(Gdx.files.internal("bac_scream.wav"));
		byeHole = Gdx.audio.newSound(Gdx.files.internal("hole_bye.wav"));
		music = Gdx.audio.newMusic(Gdx.files.internal("gamesound.mp3"));
        music.setLooping(true);
        music.setVolume(0.5f);
        endSound = Gdx.audio.newSound(Gdx.files.internal("55001__stib__uh-oh.mp3"));
	}
	
	public void playMusic() {
		music.play();
	}
	
	public void playEndSound() {
		endSound.play();
	}
	
	public void playBacScream() {
		bacScream.play();
	}
	
	public void playByeHole() {
		byeHole.play();
	}
	
}