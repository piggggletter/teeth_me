package com.mygdx.game;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;

public class Hole {
	
	 private int [][] positionHole;
	 private int [][] showHole;
	 private Random randPosHole;
	 private float time = 0;
	 private int countHole = 0;
	 private boolean startCount = false;
	 private int positionRand = 0;
	 private SoundEffect sound;
	 
	 public Hole() {
		 sound = new SoundEffect();
		 randPosHole =new Random();
		 showHole = new int [2][12];
		 positionHole = new int [][]{
			 {140, 201, 305, 419, 510, 580, 515, 450, 395, 330, 270, 220},
			 {401, 380, 380, 375, 375, 380, 156, 140, 130, 140, 145, 160}
	     };    
	 }
	 private void randomPos() {
	    	 positionRand = randPosHole.nextInt(12);
	    	 showHole[0][countHole] = positionHole[0][positionRand];
	    	 showHole[1][countHole] = positionHole[1][positionRand];
	    	 	countHole++;
	    	 	if (countHole == 12) {
	    	 		countHole = 0;
	    	 	}
	 }
	    
	 public void update(float delta) {
	    		if((time >= 7) && !startCount) {
	    			startCount = true;
	    		}else if(startCount) {
	    			if(time >= 1) {
	    				randomPos();
	    				time = 0;
	    			}else {
	    				time += delta;
	    			}
	    		}else {
    				time += delta;
    			}
	 }
	    
	 public int[][] getShowHole() {
	    		return showHole;
	 }
	   
	 public void goodbyeHole(float xt, float yt) {
	    	for (int i = 0 ; i < 12 ; i++ ) {
	    		if ((xt  > showHole[0][i]) && (xt < showHole[0][i] + 70)) {
	    			if ((600 - yt  > showHole[1][i]) && (600 - yt < showHole[1][i] + 64)) {
	    				if (Gdx.input.isButtonPressed(Buttons.LEFT)) {
	    					if(GameScreen.toolsCheck == 1){
	    						sound.playByeHole();
	    						showHole[0][i] = 0;
		    					showHole[1][i] = 0;	
	    					}
	    				}
	    			}
	    		}
	    	}
	 }

}