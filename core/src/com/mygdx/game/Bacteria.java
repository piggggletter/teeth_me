package com.mygdx.game;

import java.util.Random;

public class Bacteria {
	
	    private int [][] positionBac; 
	    private float time = 0;
	    private int countBac = 0;
	    private SoundEffect sound;
	    private Random randBacPos;
	    
	    public Bacteria() {
		 positionBac = new int [2][10];
		 sound = new SoundEffect();
		 randBacPos = new Random();
	    }
	   
	    private void randomPos() {
	    	 positionBac[0][countBac] = randBacPos.nextInt(350) + 200;
	    	 positionBac[1][countBac] = randBacPos.nextInt(200) + 200;
	    	 	countBac++;
	    	 	if (countBac == 10) {
	    	 		countBac = 0;
	    	 	}
	    }
	    
	    public void update(float delta) {
	    		if (time >= 1) {
	    			randomPos();
	    			time = 0;
	    		}else {
	    			time += delta;
	    		}
	    }
	    
	    public int[][] getBacPosition() {
	    		return positionBac;
	    }
	    
	    public void goodbyeBacteria(float xt , float yt) {
	    		for (int i = 0 ; i < 10 ; i++ ) {
	    			if ((xt  > positionBac[0][i]) && (xt < positionBac[0][i] + 80)) {
	    				if ((600 - yt  > positionBac[1][i]) && (600 - yt < positionBac[1][i] + 80)) {
	    					if(GameScreen.toolsCheck == 0){
	    						positionBac[0][i] = 0;
	    						positionBac[1][i] = 0; 
	    						sound.playBacScream();
	    					}
	    				} 
	    			}
	    		}
	    }

}