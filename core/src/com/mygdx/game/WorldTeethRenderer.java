package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class WorldTeethRenderer {

	private World world;
	private TeethMeGame teethMeGame;
    private Texture toothBrushImg;
    private Texture teethImg;
    public static Texture bacteriaImg;
    private Texture pliersImg;
    private Texture holeImg;
    private Texture heartImg;
    private Texture gameOverImg;
    private Tools toothBrush;
    private Tools pliers;
    private Bacteria bacteria;
    private Hole hole;
    private int [][] bacpos;
    private int [][] holepos;
    private BitmapFont font;
    private SoundEffect sound;
    private int checkSound = 0;

    public WorldTeethRenderer(TeethMeGame teethMeGame, World world) {
        this.teethMeGame = teethMeGame;
        this.world = world;
        holepos = new int [2][12];
        sound = world.getSound();
        toothBrushImg = new Texture("website-disabled-4VmYGw-clipart.GIF");
        teethImg = new Texture("Cartoon-mouth-and-teeth-vector-set-04.jpg");
        bacteriaImg = new Texture("Germ-PowerPoint-499x450.png");
        pliersImg = new Texture("pliers-clipart-pliers-clipart-rXSEy9-clipart.png");
        holeImg = new Texture("nTBX74Myc.png");
        heartImg = new Texture("heart.png");
        gameOverImg = new Texture("gameover .png");
        toothBrush = world.getToothBrush();
        pliers = world.getPliers();
        hole = world.getHole();
        bacteria = world.getBacteria();
        bacpos = bacteria.getBacPosition();
        holepos = hole.getShowHole();
        font = new BitmapFont();
        sound.playMusic();

    }

    public void render(float delta) {
        SpriteBatch batch = teethMeGame.batch;
        batch.begin();
        batch.draw(teethImg, 0, 0);
        batch.draw(heartImg, 20, 20);
        if(world.isEnd()) {
        		batch.draw(gameOverImg, 150, 200);
        		if(checkSound == 0) {
        			sound.playEndSound();
        			checkSound = 1;
        		}
        }else {
        		font.draw(batch, "" + world.heartRate, 65, 65);
       		for (int i = 0; i < 10; i++) {
       			if ((bacpos[0][i] != 0) && (bacpos[1][i] !=0)) {
       				batch.draw(bacteriaImg, bacpos[0][i], bacpos[1][i]);
       			}
       		}
       		for(int i = 0; i < 12; i++) {
       			if ((holepos[0][i] != 0) && (holepos[1][i] !=0)) {
       				batch.draw(holeImg, holepos[0][i], holepos[1][i]);
       			}
       		} 
    			if(GameScreen.toolsCheck == 1) {
    				batch.draw(pliersImg, pliers.getPosition().x, 600 - pliers.getPosition().y);
    			}else if(GameScreen.toolsCheck == 0) {
    				batch.draw(toothBrushImg, toothBrush.getPosition().x, 600 - toothBrush.getPosition().y);
    			}
        }
       batch.end();
    }
    
}